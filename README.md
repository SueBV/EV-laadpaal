Snow EV-meter, opensource thuislaad oplossing voor alle EV
berijders. Gebouwd met een Unix opensource OS, zelf ontwikkelde
kernel-module en standaard verkrijgbare hardware componenten voor het
nauwkeurig meten van gebruikte energie.

Naast het voorzien in het laden van een EV, wordt er een
declaratieverzoek naar de werkgever verstuurd voor de verrekening van
geleverde (privé) energie (kWh). De componenten voor het meten van de
geleverde energie voldoen aan de eisen van de werkgever en de fiscus,
hierdoor accepteert de belastingdienst de metingen en declaraties.
