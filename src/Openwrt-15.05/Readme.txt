OpenWRT download location: 
           https://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/

download the SDK builder
           OpenWrt-ImageBuilder-15.05.1-ar71xx-generic.Linux-x86_64.tar.bz2

 unpack (install) and review https://wiki.openwrt.org/doc/howto/buildroot.exigence
 and start building an image on a linux system (ubuntu) with 'make'
 you find an openwrt image in bin/ar71xx

to make an image for the EV-meter hardware:
now copy the src/
use the dot.config file from the git src/Openwrt-15.05 directory to compile an image,
this will be your .config file needed for menuconfig
copy the src/Openwrt-15.05/files directory in the root of builder directory
copy the kmod-gpio-S0 and semd-v1.1 directories in builder_root/package directory
now do a 'make menuconfig', save and start a build with 'make'

ted@vm03-ts-devel:~/openwrt-15.05$ make
 make[1] world
 make[2] target/compile
 make[3] -C target/linux compile
 make[2] package/cleanup
 make[2] package/compile
 make[3] -C package/libs/toolchain compile
 make[3] -C package/libs/libnl-tiny compile
 make[3] -C package/libs/libjson-c compile
 make[3] -C package/utils/lua compile
 make[3] -C package/libs/libubox compile
 make[3] -C package/system/ubus compile
 make[3] -C package/system/uci compile
 make[3] -C package/network/config/netifd compile
 make[3] -C package/system/opkg host-compile
 make[3] -C package/system/ubox compile
 make[3] -C package/libs/lzo compile
 make[3] -C package/libs/zlib compile
 make[3] -C package/libs/ncurses host-compile
 make[3] -C package/libs/ncurses compile
 make[3] -C package/utils/util-linux compile
 make[3] -C package/utils/ubi-utils compile
 make[3] -C package/system/procd compile
 make[3] -C package/system/usign host-compile
 make[3] -C package/utils/jsonfilter compile
 make[3] -C package/system/usign compile
 make[3] -C package/base-files compile
 make[3] -C package/boot/uboot-ar71xx compile
 make[3] -C package/system/fstools compile
 make[3] -C package/boot/uboot-envtools compile
 make[3] -C feeds/luci/modules/luci-base host-compile
 make[3] -C package/firmware/linux-firmware compile
 make[3] -C package/kernel/linux compile
 make[3] -C package/network/utils/iptables compile
 make[3] -C package/network/config/firewall compile
 make[3] -C package/utils/lua host-compile
 make[3] -C feeds/luci/applications/luci-app-firewall compile
 make[3] -C feeds/luci/libs/luci-lib-ip compile
 make[3] -C feeds/luci/libs/luci-lib-nixio compile
 make[3] -C package/network/utils/iwinfo compile
 make[3] -C package/system/rpcd compile
 make[3] -C feeds/luci/modules/luci-base compile
 make[3] -C feeds/luci/modules/luci-mod-admin-full compile
 make[3] -C feeds/luci/protocols/luci-proto-ppp compile
 make[3] -C feeds/luci/themes/luci-theme-bootstrap compile
 make[3] -C package/libs/polarssl compile
 make[3] -C package/libs/ustream-ssl compile
 make[3] -C package/network/services/uhttpd compile
 make[3] -C feeds/luci/protocols/luci-proto-ipv6 compile
 make[3] -C feeds/luci/collections/luci compile
 make[3] -C /home/ted/i2c-tools compile
 make[3] -C package/kernel/gpio-button-hotplug compile
 make[3] -C package/kernel/i2c-gpio-custom compile
 make[3] -C package/firmware/ath10k-firmware compile
 make[3] -C package/firmware/b43legacy-firmware compile
 make[3] -C package/libs/ocf-crypto-headers compile
 make[3] -C package/libs/openssl compile
 make[3] -C package/network/services/hostapd compile
 make[3] -C package/network/utils/iw compile
 make[3] -C package/kernel/mac80211 compile
 make[3] -C /home/ted/kmod-gpio-S0 compile
 make[3] -C package/libs/libusb compile
 make[3] -C package/network/config/swconfig compile
 make[3] -C package/network/ipv6/odhcp6c compile
 make[3] -C package/network/services/dnsmasq compile
 make[3] -C package/network/services/dropbear compile
 make[3] -C package/network/services/odhcpd compile
 make[3] -C package/libs/libpcap compile
 make[3] -C package/network/utils/linux-atm compile
 make[3] -C package/network/utils/resolveip compile
 make[3] -C package/network/services/ppp compile
 make[3] -C /home/ted/semd-v1.1 compile
 make[3] -C package/system/mtd compile
 make[3] -C package/system/opkg compile
 make[3] -C package/utils/busybox compile
 make[3] -C package/utils/e2fsprogs compile
 make[3] -C package/utils/usbutils compile
 make[2] package/install
 make[3] package/preconfig
 make[2] target/install
 make[3] -C target/linux install
 make[3] -C target/sdk install
 make[3] -C target/imagebuilder install
 make[2] package/index

ted@vm03-ts-devel:~/openwrt-15.05$ ls  bin/ar71xx/
md5sums
openwrt-ar71xx-generic-gl-inet-6408A-v1-squashfs-factory.bin
openwrt-ar71xx-generic-gl-inet-6408A-v1-squashfs-sysupgrade.bin
openwrt-ar71xx-generic-gl-inet-6416A-v1-squashfs-factory.bin
openwrt-ar71xx-generic-gl-inet-6416A-v1-squashfs-sysupgrade.bin
openwrt-ar71xx-generic-nbg460n_550n_550nh-u-boot.bin
openwrt-ar71xx-generic-root.squashfs
openwrt-ar71xx-generic-root.squashfs-64k
openwrt-ar71xx-generic-uImage-gzip.bin
openwrt-ar71xx-generic-uImage-lzma.bin
openwrt-ar71xx-generic-vmlinux.bin
openwrt-ar71xx-generic-vmlinux.elf
openwrt-ar71xx-generic-vmlinux.gz
openwrt-ar71xx-generic-vmlinux.lzma
openwrt-ar71xx-generic-vmlinux-lzma.elf
OpenWrt-ImageBuilder-ar71xx-generic.Linux-x86_64.tar.bz2
OpenWrt-SDK-ar71xx-generic_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64.tar.bz2
packages
sha256sums
uboot-ar71xx-nbg460n_550n_550nh
