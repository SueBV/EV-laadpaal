/*
 *  Custom GPIO-based S0 pulse counter driver
 *
 *  Copyright (C) 2016 Ted Schipper <t.schipper@tsux.org>
 *
 * ---------------------------------------------------------------------------
 *
 *  The behaviour of this driver can be altered by setting some parameters
 *  from the insmod command line.
 *
 *  The following parameters are adjustable:
 *
 *      gpio-input# <start_cnt> <min_pht> <min_plt>
 *
 *  where:
 *
 *  gpio        GPIO pin used for the S0 input (required)
 *  <start_cnt> SO count start value (default = 0)
 *  <min_pht>   minimum pulse high time (high debounce, default = 20 (ms)) 
 *  <min_plt>   minimum pulse low time (low debounce, default = 20 (ms))
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/timer.h>


#define DRV_NAME        "gpio-S0"
#define DRV_DESC        "Custom GPIO-based S0 pulse count driver"
#define DRV_VERSION     "0.0.3"

#define PFX             DRV_NAME ":"

#define CML_PARAM_GPIO          0
#define CML_PARAM_CNT           1
#define CML_PARAM_PHT           2
#define CML_PARAM_PLT           3

#define CML_PARAM_REQUIRED      1
#define CML_PARAM_COUNT         5

#define CML_PARM_DESC \
        " config -> gpio,[start_cnt,min_pht,min_plt]"

/*
 * Module commandline arguments
 */
static int S0_gpio = -1;
static unsigned long S0_cnt = 0;
static int S0_pht = 20;
static int S0_plt = 20;
static int S0_ttot = 0;
static int S0_tt0 = 0;
static int S0_tt1 = 0;
static int S0_errcnt = 0;
static unsigned long S0_ptim = 0;

module_param(S0_gpio, int, 0644);
MODULE_PARM_DESC(S0_gpio, "GPIO pin used for the S0 input");
module_param(S0_cnt, long, 0644);
MODULE_PARM_DESC(S0_cnt, "SO count start value");
module_param(S0_pht, int, 0644);
MODULE_PARM_DESC(S0_pht, "minimum pulse high time");
module_param(S0_plt, int, 0644);
MODULE_PARM_DESC(S0_plt, "minimum pulse low time");
module_param(S0_ttot, int, 0644);
MODULE_PARM_DESC(S0_ttot, "measured pulse time");
module_param(S0_tt0, int, 0644);
MODULE_PARM_DESC(S0_tt0, "measured pulse low time");
module_param(S0_tt1, int, 0644);
MODULE_PARM_DESC(S0_tt1, "measured pulse high time");
module_param(S0_errcnt, int, 0644);
MODULE_PARM_DESC(S0_errcnt, "measured pulse errors");
module_param(S0_ptim, int, 0644);
MODULE_PARM_DESC(S0_ptim, "ticks since last measured pulse");

static struct timer_list S0_timer;    /* thread timer */
static unsigned int tim_res = 0;
static unsigned int tx, t0, t1, ttot, tt0, tt1; /* pulse timing vars */
static unsigned int calc, st, cycl, tim, dtim;
static int tpht, tplt;


/*
 * The S0_timer_func, called when the S0_timer expires
 */
static void S0_timer_func(unsigned long data)
{
 static int level;

 tim++;
 dtim++;
 S0_ptim++;
 tx = tim;
 level = gpio_get_value(S0_gpio); /* S0 input level */
 S0_timer.expires = jiffies + 1; /* 10ms ? */
 add_timer(&S0_timer); /* restart timer */

 /* detect and measure high pulse */
 if ((st == 0) && (level > 0))  /* low to high transition */
 {
    st = 1;
    t0 = tx; /* record pulse low time */
    tim = 0; /* restart out timer */
    ++cycl;
 }

 /* detect and measure low pulse */
 if ((st == 1) && (level == 0) && (cycl > 0)) /* high to low transition */
 {
    st = 0;
    t1 = tx; /* record pulse time high time */
    tim = 0; /* restart timer */
    ++cycl;
 }

 if (cycl >= 3) /* complete cycle measured */
 {
    S0_ttot = ttot = ((t0 + t1) * 1000) / tim_res; /* cnt => ms */
    S0_tt0 = tt0 = (t0 * 1000) / tim_res;          /* cnt => ms */
    S0_tt1 = tt1 = (t1 * 1000) / tim_res;          /* cnt => ms */
    if ((t1 >= tpht) && (t0 >= tplt)){ /* pulse in range ? */
       ++S0_cnt; /* add to the count */
       S0_ptim = 0; /* reset the tick cnt */
    } else {
      S0_errcnt++;  /* update the error cnt */
    }

    cycl = 1;
    ++calc;
 }

 if (ttot > 0 && calc > 0 && dtim > 1000) {
    printk(KERN_INFO "S0 pulse: tt0=%d, tt1=%d, ttot=%d, S0_cnt=%d, S0_errcnt=%d\n", tt0, tt1, ttot, S0_cnt, S0_errcnt);
    calc = 0;
    dtim = 0;
 }
}


static int gpio_S0_setup(void)
{
 int ret = 0;
 unsigned long i;

 printk(KERN_INFO DRV_DESC " version " DRV_VERSION "\n");
 if (S0_gpio < 0) {
    printk(KERN_ERR PFX "no GPIO input specified\n");
    return(-ENODEV);
 }
 ret = gpio_request_one(S0_gpio, GPIOF_IN, "S0-input");
 if (ret) {
    printk(KERN_ERR "Unable to request GPIO for S0 input\n");
    goto fail1;
 }

 i = jiffies;
 mdelay(1000);
 tim_res = jiffies - i; /* get the # jiffies per second */
 printk(KERN_INFO "timer resolution = %d jiffies / second\n", tim_res);
 printk(KERN_INFO "kernel value HZ is set to: %d \n", HZ);

 /* init pulse variables */
 tx = t0 = t1 = ttot = tt0 = tt1 = 0;
 calc = st = cycl = tim = dtim = 0;
 tpht = tplt = 1;
 S0_errcnt = 0;

 /* calc minimum cycly time values */
 tpht = ((tim_res * 100) * S0_pht) / 100000;
 if (tpht <= 0)
    tpht = 1;
 tplt = ((tim_res * 100) * S0_plt) / 100000;
 if (tplt <= 0)
    tplt = 1;

 /* init timer and add timer function */
 init_timer(&S0_timer);
 S0_timer.function = S0_timer_func;
 S0_timer.data = 1L;
 S0_timer.expires = jiffies + (2*HZ);  /* 2 sec */
 add_timer(&S0_timer);

 return(0);

 fail2:
    del_timer_sync(&S0_timer); 
    gpio_free(S0_gpio);
 fail1:
    return(ret);
}

static void gpio_S0_cleanup(void)
{
 printk(KERN_INFO "gpio-S0 exit\n");
 del_timer_sync(&S0_timer); 
 gpio_free(S0_gpio);
}

#ifdef MODULE
/*
 * Module init function
 */
static int __init gpio_S0_init(void)
{
 return gpio_S0_setup();
}
module_init(gpio_S0_init);

/*
 * Module init function
 */
static void __exit gpio_S0_exit(void)
{
 gpio_S0_cleanup();
}
module_exit(gpio_S0_exit);
#else
subsys_initcall(gpio_S0_setup);
#endif /* MODULE*/

// MODULE_LICENSE("BSD");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ted Schipper <t.schipper@tsux.org>");
MODULE_DESCRIPTION(DRV_DESC);
MODULE_VERSION(DRV_VERSION);

