/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/
#ifndef NFC_H
#define NFC_H

#include <nfc/nfc.h>

nfc_device * init_nfc(void);
int poll_nfc(nfc_device *dev);
int scan_nfc(nfc_device *dev);
int close_nfc(nfc_device *dev);
int get_tag_id(uint8_t * tag);
void sprint_hex(uint8_t *buf, const uint8_t *pbtData, const size_t szBytes);

#endif /* NFC_H */
