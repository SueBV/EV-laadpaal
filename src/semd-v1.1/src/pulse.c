/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/

#include "timer.h"
#include "pulse.h"

int read_pulsecnt_file(char *);
int write_pulsecnt_file(char *,unsigned int);

unsigned int pulses_kwh = 1000;   // S0 pulses per kWh
unsigned int pulse_high_len = 30; // msec
unsigned int pulse_low_len = 30;  // msec
unsigned int err_cnt = 0; // pulse error count

unsigned int tt0, tt1, ttot;
unsigned int calc;
unsigned int s0_pulse;
unsigned long load;
unsigned long ptim;
float p0;
FILE *kmod_s0_cnt = NULL;
FILE *kmod_s0_ttot = NULL;
FILE *kmod_s0_tt0 = NULL;
FILE *kmod_s0_tt1 = NULL;
FILE *kmod_s0_errcnt = NULL;
FILE *kmod_s0_ptim = NULL;

/* Paths to kernel module variables */
static char *kmod_s0_sys_cnt =  "/sys/module/gpio_S0/parameters/S0_cnt";
static char *kmod_s0_sys_ttot = "/sys/module/gpio_S0/parameters/S0_ttot";
static char *kmod_s0_sys_tt0 =  "/sys/module/gpio_S0/parameters/S0_tt0";
static char *kmod_s0_sys_tt1 =  "/sys/module/gpio_S0/parameters/S0_tt1";
static char *kmod_s0_sys_err =  "/sys/module/gpio_S0/parameters/S0_errcnt";
static char *kmod_s0_sys_ptim =  "/sys/module/gpio_S0/parameters/S0_ptim";


/**
 
 */
int pulse_init(char *file_name)
{
 int p;
 tt0 = tt1 = ttot = 0;
 calc = 0;
 load = 0;
 ptim = 0;
 p = 0;
 set_app_timer(PULSE_TIM, UINT_MAX); /* start timer */
 set_app_timer(S0_CNT_TIM,MIN10);   /* reset to 10 minutes */
 if (file_name != NULL) {
    p = read_pulsecnt_file(file_name);
    if (p < 0) {
       syslog(LOG_INFO, "Can not open pulse count file: %s, createing!",
              file_name);
       write_pulsecnt_file(file_name,0);
       p = 0;
    }
 }
 syslog(LOG_INFO, "Pulse count init to %d !", p);
 s0_pulse = p;

 kmod_s0_cnt = fopen(kmod_s0_sys_cnt,"r+");
 if (kmod_s0_cnt == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_cnt);
    return(1);
 }
 fprintf(kmod_s0_cnt, "%d\n", s0_pulse);
 fclose(kmod_s0_cnt);
 return(0);
}

/**
 
 */
void pulse_measure(void)
{
 int ret;
 int dummy;
 FILE *fd = NULL;

 fd = fopen(kmod_s0_sys_cnt,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_cnt);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0) {
       if (s0_pulse != dummy)
          calc++;
       s0_pulse = dummy;
    }
    else
       s0_pulse = 0;
    fclose(fd);
 }

 fd = NULL;
 fd = fopen(kmod_s0_sys_ttot,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_ttot);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0)
       ttot = dummy;
    else
       ttot = 0;
    fclose(fd);
 }

 fd = NULL;
 fd = fopen(kmod_s0_sys_tt0,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_tt0);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0)
       tt0 = dummy;
    else
       tt0 = 0;
    fclose(fd);
 }

 fd = NULL;
 fd = fopen(kmod_s0_sys_tt1,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_tt1);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0)
       tt1 = dummy;
    else
       tt1 = 0;
    fclose(fd);
 }

 fd = NULL;
 fd = fopen(kmod_s0_sys_err,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_err);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0)
       err_cnt = dummy;
    else
       err_cnt = 0;
    fclose(fd);
 }

 fd = NULL;
 fd = fopen(kmod_s0_sys_ptim,"r");
 if (fd == NULL) {
    syslog(LOG_ERR, "Can not open: %s , kernel module gpio-S0 loaded?",kmod_s0_sys_ptim);
 } else {
    ret = fscanf(fd,"%d", &dummy);
    if (ret > 0)
       ptim = dummy;
    else
       ptim = 0;
    fclose(fd);
 }
 if (ttot > 0 && calc > 0) {
    load = ((((1000 * WHour) / pulses_kwh) * 3600) / ttot); /* calc Watt */
    calc = 0;
 }
 if (ptim > 3000) /* no pulses 30 sec */
    load = 0;
}

/**
 
 */
void pulse_update(char *file_name, int action)
{
 if (action == 0) /* immediate action required */
 {
    write_pulsecnt_file(file_name,s0_pulse);
    syslog(LOG_INFO, "Pulse count file %s action update",file_name);
    
 } else { /* timer based action */
   if (tst_app_timer(S0_CNT_TIM) == 0) { /* periodic update timer expired */
      set_app_timer(S0_CNT_TIM,MIN10);   /* reset to 10 minutes */
      write_pulsecnt_file(file_name,s0_pulse);
      syslog(LOG_INFO, "Pulse count file %s periodic update",file_name);
   }
 }
}

/**
 
 */
void printPulse(FILE *stream)
{
 float ttime;

 if (ttot > 0 && calc > 0) {
//          p0 = ((((WHour * 100) / PULSES_KWH) * 3600) / (ttot/100));
    ttime = ttot;
    p0 = (float)(((1000 * WHour) / pulses_kwh * 3600) / ttime);
//    fprintf(stream, "tt0=%d, tt1=%d, S0 cnt=%d, Ecnt=%d, P0=%f , load=%d\n",
    fprintf(stream, "tt0=%d, tt1=%d, S0 cnt=%d, Ecnt=%d, P0=%f , load=%d\n",
            tt0,tt1,s0_pulse, err_cnt, p0, load);
    calc = 0;
 }
}

/**
 * Read pulse count from storage
 */
int read_pulsecnt_file(char *file_name)
{
 FILE *conf_file = NULL;
 int ret = -1;
 int dummy;

 if (file_name == NULL) return 0;
 conf_file = fopen(file_name, "r");
 if (conf_file == NULL) {
    syslog(LOG_ERR, "Can not open pulse count file: %s, error: %s",
           file_name, strerror(errno));
    return -1;
 }
 ret = fscanf(conf_file, "%d", &dummy);
 if (ret > 0) {
    syslog(LOG_INFO, "Pulse count (%d) read from file %s", dummy,file_name);
 }
 fclose(conf_file);
 return (dummy);
}

/**
 * Write pulse count to storage
 */
int write_pulsecnt_file(char *file_name,unsigned int count)
{
 FILE *conf_file = NULL;
 int ret = -1;

 if (file_name == NULL) return 0;
 conf_file = fopen(file_name, "w");
 if (conf_file == NULL) {
    syslog(LOG_ERR, "Can not open pulse count file: %s, error: %s",
           file_name, strerror(errno));
    return -1;
 }
 ret = fprintf(conf_file, "%d\n", count);
 if (ret > 0) {
    syslog(LOG_INFO, "Pulse count (%d) written to file %s",count,file_name);
 }
 fclose(conf_file);
 return ret;
}

/**
 * Get the current load in Watt's
 */
unsigned long curr_load(void)
{
 return(load);
}

/**
 * Get the current S0 pulse count
 */
unsigned int pulse_cnt(void)
{
 return(s0_pulse);
}

/**
 * Get the current kWh count
 */
unsigned int kwh_cnt(void)
{
 return((s0_pulse * 1000) / pulses_kwh);
}

/**
 * Get the current tt0 value
 */
unsigned int get_tt0(void)
{
 return(tt0);
}

