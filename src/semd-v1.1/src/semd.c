/*
***************************************************************************
*
* Author:
*
***************************************************************************
*
*
***************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include "timer.h"
#include "gpio.h"
#include "pulse.h"
#include "nfc.h"
#include "pcf8574.h"
#include "iniparser.h"
#include "oled.h"

static int running = 0;
static int delay = 1;
static int counter = 0;
static char *count_file_name = NULL;
static char *pid_file_name = NULL;
static char *ini_file_name = NULL;
static int pid_fd = -1;
static int oled_fd = -1;
static char *app_name = NULL;
static FILE *log_stream;
static int debug = 0;

void handle_signal(int);
static void daemonize(void);

int gpioSetup(void);
extern volatile unsigned int *gpioAddr;
extern unsigned int pulses_kwh;
extern unsigned int pulse_high_len;
extern unsigned int pulse_low_len;

/**
 * Print help for this application
 */
void print_help(void)
{
   printf("\n Usage: %s [OPTIONS]\n\n", app_name);
   printf("  Options:\n");
   printf("   -h --help                 Print this help\n");
   printf("   -c --pulsecount filename  Pulse count storage file\n");
   printf("   -l --log_file  filename   Write logs to the file\n");
   printf("   -d --debug level (0-9)    Debug mode, will not daemonize\n");
   printf("   -p --pid_file  filename   PID file used by daemonized app\n");
   printf("   -i --ini_file  filename   INI file for daemonized app default settings\n");
   printf("\n");
}

/* Main function */
int main(int argc, char *argv[])
{
 static struct option long_options[] = {
    {"count_file", required_argument, 0, 'c'},
    {"log_file", required_argument, 0, 'l'},
    {"help", no_argument, 0, 'h'},
    {"debug", required_argument, 0, 'd'},
    {"pid_file", required_argument, 0, 'p'},
    {"ini_file", required_argument, 0, 'i'},
    {NULL, 0, 0, 0}
 };
 struct timeval tv = {0, 0};
 int value, option_index = 0, ret;
 char *log_file_name = NULL;
 char *i2c_file_name = "/dev/i2c-0";
 int start_daemonized = 1;
 char s[25];
 char t[25];
 char buf[5] = {0};
 int charge = 0;
 unsigned int tmp = 0;
 unsigned int ch_time = 0;
 int hr, min, sec;
 nfc_device *pnd;
 int tag = 0;
 uint8_t tag_id[10];
 uint8_t tag_id_cur[10];
 uint8_t tag_id_str[20];
 uint8_t tag_id_sz;
 uint8_t *user;
 uint8_t key[32];
 int i;
 int res;
 dictionary *ini;

 ini_file_name = "/etc/semd.ini";
 ini = iniparser_load(ini_file_name);
 if (ini == NULL) {
    fprintf(stderr, "cannot parse file: %s\n", ini_file_name);
    return -1 ;
 }
 log_file_name = iniparser_getstring(ini, "daemon:logfile", NULL);
 pid_file_name = iniparser_getstring(ini, "daemon:pidfile", NULL);
 count_file_name = iniparser_getstring(ini, "daemon:cntfile", NULL);
 pulses_kwh = iniparser_getint(ini,"daemon:pulse_cnt",1000);
 pulse_high_len = iniparser_getint(ini,"daemon:pulse_high_len",30);
 pulse_low_len = iniparser_getint(ini,"daemon:pulse_low_len",30);
   
 app_name = argv[0];
 /* Try to process all command line arguments */
 while((value = getopt_long(argc, argv, "c:l:p:d:h", long_options, &option_index)) != -1) {
    switch(value) {
       case 'c':
          count_file_name = strdup(optarg);
          break;
       case 'l':
          log_file_name = strdup(optarg);
          break;
       case 'p':
          pid_file_name = strdup(optarg);
          break;
       case 'd':
          start_daemonized = 0;
          debug = atoi(optarg);
          break;
       case 'h':
          print_help();
          return EXIT_SUCCESS;
       case '?':
          print_help();
          return EXIT_FAILURE;
       default:
          break;
    }
 }
 if (debug > 0) {
    printf("Pulse count file: [%s]\n", count_file_name ? count_file_name : "UNDEF");
    printf("Log file: [%s]\n", log_file_name ? log_file_name : "UNDEF");
    printf("Pid file: [%s]\n", pid_file_name ? pid_file_name : "UNDEF");
    printf("Pulses/kWh: [%d]\n", pulses_kwh);
    printf("Pulse high len: [%d]\n", pulse_high_len);
    printf("Pulse low len: [%d]\n", pulse_low_len);
 }
 if (start_daemonized > 0) daemonize();
 /* Open system log and write message to it */
 openlog(argv[0], LOG_PID|LOG_CONS, LOG_DAEMON);
 syslog(LOG_INFO, "Started %s", app_name);
 /* Daemon will handle two signals */
 signal(SIGINT, handle_signal);
 signal(SIGHUP, handle_signal);
 signal(SIGTERM, handle_signal);
 if (log_file_name != NULL) {   /* Try to open log file to this daemon */
    log_stream = fopen(log_file_name, "a+");
    if (log_stream == NULL) {
       syslog(LOG_ERR, "Can not open log file: %s, error: %s",
              log_file_name, strerror(errno));
       log_stream = stdout;
    }
 } else {
    log_stream = stdout;
 }
 oled_fd = oled_init(i2c_file_name,OLED_address);
 if (oled_fd < 0) {
    fprintf(log_stream, "Error on OLED I2C port: %s\n",i2c_file_name);
    exit(1);
 }
 oled_reset_display(oled_fd);
 oled_string_font8x16xy(oled_fd,0,0,"SNOW");
 oled_setpos(oled_fd,5,0);
// oled_string_font6x8(oled_fd,"Energy Monitor");
   oled_string_font6x8(oled_fd,"EV Laad Monitor");
 oled_setpos(oled_fd,8,1);
 oled_string_font6x8(oled_fd,"V0.3-PoC");
 init_app_timers();      /* initialize the application timers */
 if (init_i2c_io(i2c_file_name,PCF8574_I2C_ADDR) < 0) {
    fprintf(log_stream, "Error on PCF8574 I2C port: %s\n",i2c_file_name);
    exit(1);
 }
 init_beeber();
 running = 1; /* This global variable can be changed in the signal handler */
 set_app_timer(MLOOP_TIM,SEC_1); /* set the loop timeout timer */
 if (gpioSetup() < 0) fprintf(log_stream, "gpio setup failed\n");
 if (debug > 0) fprintf(log_stream, "gpioAddr   address = %p\n", gpioAddr);
 if (pulse_init(count_file_name) > 0) {  /* init S0 pulse counter */
    fprintf(log_stream, "S0 pulse init failed, kernel module gpio-S0 not loaded\n");
    exit(1);
 }
 pnd = init_nfc();
 if (pnd == NULL) {
    fprintf(log_stream, "Error on NFC device init\n");
    exit(1);
 }
 beeb(3,300,300);
 user = "(Free)";
 while (running == 1)   /* Never ending loop of server */
 {
    tv.tv_sec = 1; tv.tv_usec = 0;   /* set loop delay time */
    select(0, NULL, NULL, NULL, &tv) ;
    pulse_update(count_file_name,1); /* store S0 count */
    beeber_handler(); /* update the beep timing */
    if (tst_app_timer(MLOOP_TIM) == 0) { /* Timer 0 expired? do something */
        set_app_timer(MLOOP_TIM,SEC_1);
        pulse_measure(); /* sync kmod variables with pulse functions */
//#ifdef TED
        if (poll_nfc(pnd) > 0) /* NFC tag detected */
//        if (scan_nfc(pnd) > 0) /* NFC tag detected */
        {
           set_app_timer(LCD_TIM,2*SEC_1);
           tag_id_sz = get_tag_id(tag_id);
           sprint_hex(tag_id_str,tag_id,tag_id_sz);
           if (debug == 1)
              printf("NFC tag Id: %s\n",tag_id_str);
           syslog(LOG_INFO, "NFC tag Id: %s found\n", tag_id_str);
           if (tag == 0) {
              sprintf(key,"nfc-id:%s",tag_id_str);
              if ((user=iniparser_getstring(ini,key,NULL)) != NULL) {
                 if ((res = start_tr(ini,user)) > 0) {
                    tag = 1;
                    charge = 1;
                    beeb(1,500,500);
                    set_app_timer(CHG_TIM,UINT_MAX);
                    if (debug == 1)
                       printf("NFC-Id: %s , User: %s\n",tag_id_str,user);
                    syslog(LOG_INFO,"NFC-Id: %s , User: %s\n",tag_id_str,user);
                    memcpy(tag_id_cur,tag_id,tag_id_sz);
                    set_app_timer(TIMER_7,30*SEC_1);  /* disable EV load check */
                 } else {
                    printf("Start_tr err: %d\n", res);
                 }
              }
           } else {
              if (memcmp(tag_id,tag_id_cur,tag_id_sz) == 0) {
                 if ((res = stop_tr(ini,user)) > 0) {
                    write_tr(ini,user);
                    tag = 0;
                    for (i=0; i<10; i++)
                       tag_id[i] = 0;
                    charge = 0;
                    beeb(2,500,100);
                    user = "(Free)";
                    ch_time = UINT_MAX - tst_app_timer(CHG_TIM);
                 } else {
                    printf("Stop_tr err: %d\n", res);
                 }
              }
           }
        }
/* check charge session, still load from EV? */
//        if ((charge == 1) && (curr_load() == 0) && (get_tt0() > 20000)) {
        if ((charge == 1) && (curr_load() == 0) ) {
           if (tst_app_timer(TIMER_7) == 0) {
              syslog(LOG_INFO,"NFC-Id: %s , User: %s : No load, stopped\n",tag_id_str,user);
              if ((res = stop_tr(ini,user)) > 0) {
                 write_tr(ini,user);
                 tag = 0;
                 for (i=0; i<10; i++)
                    tag_id[i] = 0;
                 charge = 0;
                 beeb(3,500,100);
                 user = "(Free)";
                 ch_time = UINT_MAX - tst_app_timer(CHG_TIM);
              } else {
                 printf("Stop_tr err: %d\n", res);
              }
           }
        }
//#endif
        if (debug == 1) {
//           printGPIOreg(log_stream);
           printPulse(log_stream);
//           if (LCD_read(lcd_fd,buf) == 4)
//               fprintf(log_stream, "Keypad: %0x, %0x, %0x, %0x\n", buf[0],buf[1],buf[2],buf[3]);
        }
        OUT_GPIO(22); /* gpio 22 LED output */
        TGL_GPIO(22); /* hart beat LED */
    }
    if (tst_app_timer(LCD_TIM) == 0) { /* Timer 3 expired? refresh display */
       set_app_timer(LCD_TIM,SEC10);
       OUT_GPIO(21); /* gpio 21 SSR output */
       oled_string_font8x16xy(oled_fd,0,0,"SNOW");
       oled_setpos(oled_fd,5,0);
//       oled_string_font6x8(oled_fd,"Energy Monitor");
       oled_string_font6x8(oled_fd,"EV Laad Monitor");
       oled_setpos(oled_fd,8,1);
       oled_string_font6x8(oled_fd,"V0.3-PoC");
       sprintf(s,"ID: %-16s",user);
       oled_setpos(oled_fd,1,3);
       oled_string_font6x8(oled_fd,s);
       sprintf(s,"C:%.6d  Watt:%.5d ",kwh_cnt(),curr_load());
       oled_setpos(oled_fd,1,5);
       oled_string_font6x8(oled_fd,s);
       if (charge == 0) {
          sprintf(s,"Ch: Off            ");
          CLR_GPIO(21);  /* SSR Off */
       } else {
          tmp = UINT_MAX - tst_app_timer(CHG_TIM);
          hr = min = sec = 0;
          if (tmp >= 3600000) {
             hr = tmp/3600000;
             tmp = tmp%3600000;
          }
          if (tmp >= 60000) {
             min = tmp/60000;
             tmp = tmp%60000;
          }
          if (tmp >= 1000) {
             sec = tmp/1000;
          }
          sprintf(s,"Ch: On , %.2d:%.2d:%.2d  ",hr,min,sec);
          SET_GPIO(21);  /* SSR On */
       }
       oled_setpos(oled_fd,1,7);
       oled_string_font6x8(oled_fd,s);
    }
//    tv.tv_sec = 1; tv.tv_usec = 0;   /* set loop delay time */
//    select(0, NULL, NULL, NULL, &tv) ;
 } // end while
 if (tag != 0) {  // transaction still busy
    stop_tr(ini,user);
    write_tr(ini,user);
 }
 CLR_GPIO(21);  /* SSR Off */
 CLR_GPIO(22);  /* HB Off */
 pulse_update(count_file_name,0); /* store S0 count */
 oled_displayOff(oled_fd);
 close_nfc(pnd);
 stop_timer();   /* stop application timers */
 if (log_stream != stdout) {
    fclose(log_stream);   /* Close log file, when it is used. */
 }
 syslog(LOG_INFO, "Stopped %s", app_name);
 closelog();     /* Write system log and close it. */
 /* Free allocated memory */
 if(count_file_name != NULL) free(count_file_name);
 if(log_file_name != NULL) free(log_file_name);
 if(pid_file_name != NULL) free(pid_file_name);
 return EXIT_SUCCESS;
}

/**
 * Callback function for handling signals.
 * param   sig   identifier of signal
 */
void handle_signal(int sig)
{
 if (sig == SIGINT) {
    fprintf(log_stream, "Debug: stopping daemon ...\n");
    if (pid_fd != -1) {          /* Unlock and close lockfile */
       lockf(pid_fd, F_ULOCK, 0);
       close(pid_fd);
    }
    if (pid_file_name != NULL) { /* Try to delete lockfile */
       unlink(pid_file_name);
    }
    running = 0;
    signal(SIGINT, SIG_DFL);    /* Reset signal handling to default */
 } else {
    if ((sig == SIGHUP) || (sig == SIGTERM)) {
       fprintf(log_stream, "Debug: received SIGHUP/TERM signal\n");
       running = 0;
    } else {
       if (sig == SIGCHLD)
          fprintf(log_stream, "Debug: received SIGCHLD signal\n");
    }
 }
}

/**
 * This function will daemonize this app
 */
static void daemonize(void)
{
 pid_t pid = 0;
 int fd;

 pid = fork();        /* Fork off the parent process */
 if (pid < 0) exit(EXIT_FAILURE);
 if (pid > 0) exit(EXIT_SUCCESS); /* Success: Let the parent terminate */
 if (setsid() < 0) exit(EXIT_FAILURE);
 /* On success: The child process becomes session leader */
 signal(SIGCHLD, SIG_IGN); /* Ignore signal from child to parent process */
 pid = fork(); /* Fork off for the second time*/
 if (pid < 0) exit(EXIT_FAILURE);
 if (pid > 0) exit(EXIT_SUCCESS); /* Success: Let the parent terminate */
 umask(0); /* Set new file permissions */
 /* Change the working directory to the root directory */
 /* or another appropriated directory */
 chdir("/");
 /* Close all open file descriptors */
 for (fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--) {
    close(fd);
 }
 /* Reopen stdin (fd = 0), stdout (fd = 1), stderr (fd = 2) */
 stdin = fopen("/dev/null", "r");
 stdout = fopen("/dev/null", "w+");
 stderr = fopen("/dev/null", "w+");
 if (pid_file_name != NULL) {  /* Try to write PID of daemon to lockfile */
    char str[256];
    pid_fd = open(pid_file_name, O_RDWR|O_CREAT, 0640);
    if (pid_fd < 0) exit(EXIT_FAILURE);  /* Can't open lockfile */
    if (lockf(pid_fd, F_TLOCK, 0) < 0) exit(EXIT_FAILURE); /* Can't lock file */
    sprintf(str, "%d\n", getpid());  /* Get current PID */
    write(pid_fd, str, strlen(str)); /* Write PID to lockfile */
 }
}

