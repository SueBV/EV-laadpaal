/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/
#ifndef TIMER_H
#define TIMER_H

#include <stdio.h>
#ifdef __linux__
#include <sys/time.h>
#include <signal.h>

int init_app_timers(void);
void timer_handler(void);
int tst_app_timer(int);
void wait_app_timer(int);
int set_app_timer(int, unsigned int);
int start_timer(int, void (*)(void));
void stop_timer(void);

#define RES_TIMERS 1   /* application timer resolution, 1msec */
#define NUM_TIMERS 10  /* number of application timers */

#define TIMER_0    0   /* timer# */
#define MLOOP_TIM  0   // main loop timer

#define TIMER_1    1
#define PULSE_TIM  1   // pulse measure timer 

#define TIMER_2    2
#define S0_CNT_TIM 2   // S0 count file update timer

#define TIMER_3    3
#define LCD_TIM    3   // LCD refresh timer

#define TIMER_4    4
#define CHG_TIM    4   // charge duration timer

#define TIMER_5    5
#define NFC_TIM    5   // NFC I2C response timer, used in libnfc (pn532_i2c.c)

#define TIMER_6    6
#define BEEP_TIM   6   // Beep duration timer

#define TIMER_7    7
#define TIMER_8    8
#define TIMER_9    9
#define SEC_1     1000        /* timer values, 1 second, etc */
#define SEC10    (SEC_1 * 10)
#define SEC60    (SEC_1 * 60)
#define MIN_1    SEC60
#define MIN10    (MIN_1 * 10)
#define MIN60    (MIN_1 * 60)
#define HOUR_1   MIN60

#endif

#endif /* TIMER_H */
