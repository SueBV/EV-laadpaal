/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/
#ifndef PULSE_H
#define PULSE_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>

int pulse_init(char *);
void pulse_measure(void);
void pulse_update(char *, int);
void printPulse(FILE *);
unsigned long curr_load(void);
unsigned int pulse_cnt(void);
unsigned int kwh_cnt(void);
unsigned int get_tt0(void);

#define WHour 1000         // kWhour = 1000 Whour
//#define PULSES_KWH  1000   // pulses / kWh

#endif /* PULSE_H */
