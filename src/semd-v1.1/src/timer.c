/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/

#include "timer.h"

void timer_sig_handler(int);
void (*timer_func_handler_pntr)(void);
// void pulse_measure(void);

struct itimerval timervalue;
struct sigaction new_handler, old_handler;
static unsigned int timer[NUM_TIMERS];

#define DELAY 1   // wait delay time, 1ms
const struct timespec Delay = {
   .tv_sec =0,
   .tv_nsec = DELAY * 1000 * 1000
};


/**
 Initialize application timer service
 */
int init_app_timers(void)
{
 int i;

 for (i=0; i<sizeof(timer); i++) {
    timer[i] = 0;
 }
 if (start_timer(RES_TIMERS, &timer_handler)) {
    printf("\n timer error\n");
    return(1);
 }
 return(0);
}

/**
 * Application timer service routine
 */
void timer_handler(void)
{
 int i;

 for (i=0; i<sizeof(timer); i++) {
    if (timer[i] > 0)
       --(timer[i]);
 }
// pulse_measure(); /* measure the S0 pulses during interrupt */
}

/**
 * Read and return application timer with requested index
 */
int tst_app_timer(int index)
{
 if (index >= 0 && index < NUM_TIMERS)
    return(timer[index]);
 return(0);
}

/**
 * Write value to application timer with requested index
 */
int set_app_timer(int index, unsigned int value)
{
 if (index >= 0 && index < NUM_TIMERS) {
    timer[index] = value;
    return(0);
 }
 return(1);
}

/**
 * Wait while timer with requested index expires
 */
void wait_app_timer(int index)
{
 if (index >= 0 && index < NUM_TIMERS) {
    do {
       nanosleep(&Delay, (struct timespec *) NULL);
    } while(timer[index] > 0);
 }
}

/**
 Initialize timer interrupt and service routine
 */
int start_timer(int mSec, void (*timer_func_handler)(void))
{
 timer_func_handler_pntr = timer_func_handler;
 timervalue.it_interval.tv_sec = mSec / 1000;
 timervalue.it_interval.tv_usec = (mSec % 1000) * 1000;
 timervalue.it_value.tv_sec = mSec / 1000;
 timervalue.it_value.tv_usec = (mSec % 1000) * 1000;
 if (setitimer(ITIMER_REAL, &timervalue, NULL)) {
    printf("\nsetitimer() error\n");
    return(1);
 }
 new_handler.sa_handler = &timer_sig_handler;
 new_handler.sa_flags = SA_NOMASK;
 if (sigaction(SIGALRM, &new_handler, &old_handler)) {
    printf("\nsigaction() error\n");
    return(1);
 }
 return(0);
}

/**
 Timer interrupt service routine
 */
void timer_sig_handler(int arg)
{
 timer_func_handler_pntr();
}

/**
 Disable timer interrupt and service routine
 */
void stop_timer(void)
{
 timervalue.it_interval.tv_sec = 0;
 timervalue.it_interval.tv_usec = 0;
 timervalue.it_value.tv_sec = 0;
 timervalue.it_value.tv_usec = 0;
 setitimer(ITIMER_REAL, &timervalue, NULL);
 sigaction(SIGALRM, &old_handler, NULL);
}

