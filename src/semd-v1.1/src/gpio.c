/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/

#include "gpio.h"

volatile unsigned int *gpioAddr;

/**
 Initialize AR9331 GPIO direct memory access
 */
int gpioSetup(void)
{
 int  m_mfd;

 if ((m_mfd = open("/dev/mem", O_RDWR)) < 0)  return -1; 
 gpioAddr = (unsigned int*)mmap(NULL, GPIO_BLOCK, PROT_READ|PROT_WRITE, MAP_SHARED, m_mfd, GPIO_ADDR);
 close(m_mfd);
 if (gpioAddr == MAP_FAILED) return -2;
 
 return 0;
}


/**
 
 */
char *int2bin(unsigned n, char *buf)
{
#define BITS (sizeof(n) * CHAR_BIT)

//    static char static_buf[BITS + 1];
 static char static_buf[BITS + 4];
 int i;
 int j;

 if (buf == NULL)
    buf = static_buf;
 for (j = BITS+2, i = BITS - 1; i >= 0; --i,--j) {
     buf[j] = (n & 1) ? '1' : '0';
     if (j>0) {
        if (i%8 == 0) {
           --j;
           buf[j] = ':';
        }
     }
     n >>= 1;
 }
 buf[BITS+3] = '\0';
 return buf;
#undef BITS
}

/**
 
 */
void printGPIOreg(FILE *stream)
{
 fprintf(stream, "          : 33222222:22221111:11111000:00000000 \n");
 fprintf(stream, "          : 10987654:32109876:54321098:76543210 \n");
 fprintf(stream, "--------- : --------:--------:--------:-------- \n");
 fprintf(stream, "Reg    OE : %s \n", int2bin(*(gpioAddr+GPIO_OE),NULL));
 fprintf(stream, "Reg    IN : %s \n", int2bin(*(gpioAddr+GPIO_IN),NULL));
 fprintf(stream, "Reg   OUT : %s \n", int2bin(*(gpioAddr+GPIO_OUT),NULL));
 fprintf(stream, "Reg   SET : %s \n", int2bin(*(gpioAddr+GPIO_SET),NULL));
 fprintf(stream, "Reg CLEAR : %s \n\n", int2bin(*(gpioAddr+GPIO_CLEAR),NULL));
}

