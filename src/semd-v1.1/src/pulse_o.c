/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/

#include "gpio.h"
#include "timer.h"
#include "pulse.h"

int read_pulsecnt_file(char *);
int write_pulsecnt_file(char *,unsigned int);

extern volatile unsigned int *gpioAddr;

unsigned int pulses_kwh = 1000;   // S0 pulses per kWh
unsigned int pulse_high_len = 30; // msec
unsigned int pulse_low_len = 30;  // msec
unsigned int err_cnt = 0; // pulse error count

unsigned int tx, t0, t1, ttot, tt0, tt1;
unsigned int calc, st, cycl;
unsigned int s0_pulse;
unsigned long load;
float p0;

/**
 
 */
void pulse_init(char *file_name)
{
 int p;
 tx = t0 = t1 = ttot = tt0 = tt1 = 0;
 calc = st = cycl = 0;
 load = 0;
 p = 0;
 INP_GPIO(18); /* gpio 18 S0 input */
 set_app_timer(PULSE_TIM, UINT_MAX); /* start timer */
 set_app_timer(S0_CNT_TIM,MIN10);   /* reset to 10 minutes */
 if (file_name != NULL) {
    p = read_pulsecnt_file(file_name);
    if (p < 0) {
       syslog(LOG_INFO, "Can not open pulse count file: %s, createing!",
              file_name);
       write_pulsecnt_file(file_name,0);
       p = 0;
    }
 }
 syslog(LOG_INFO, "Pulse count init to %d !", p);
 s0_pulse = p;
}

/**
 
 */
void pulse_measure(void)
{
 int level;

 level = GET_GPIO(18);
 tx = UINT_MAX - tst_app_timer(PULSE_TIM);  /* get the current measure time */

/* detect and measure high pulse */
 if ((st == 0) && (level > 0)) /* low to high transition */
 {
    st = 1;
    t0 = tx; /* record pulse low time */
    set_app_timer(PULSE_TIM, UINT_MAX); /* start timer */
    ++cycl;
 }

/* detect and measure low pulse */
 if ((st == 1) && (level == 0) && (cycl > 0)) /* high to low transition */
 {
    st = 0;
    t1 = tx; /* record pulse high time */
    set_app_timer(PULSE_TIM, UINT_MAX); /* start timer */
    ++cycl;
 }

 if (cycl >= 3) /* complete cycle measured */
 {
    ttot = t0 + t1;
    tt0 = t0;
    tt1 = t1;
//    if ((t1 > 30) && (t0 > 30)){ /* high and low time at least 30ms */
    if ((t1 > pulse_high_len) && (t0 > pulse_low_len)){ /* pulse in range ? */
       ++s0_pulse; /* add to the count */
//       load = ((((1000 * WHour) / PULSES_KWH) * 3600) / tt0); /* calc Watt */
       load = ((((1000 * WHour) / pulses_kwh) * 3600) / ttot); /* calc Watt */
    } else {
      err_cnt++;  /* update the error cnt */
    }
    cycl = 1;
    ++calc;
 }

 if ((st == 0) && (level == 0) && (tx > 3600000)) /* no pulse seen for 1h */
 {
    load = 0;
 }
 INP_GPIO(18); /* gpio 18 S0 input */
}

/**
 
 */
void pulse_update(char *file_name, int action)
{
 if (action == 0) /* immediate action required */
 {
    write_pulsecnt_file(file_name,s0_pulse);
    syslog(LOG_INFO, "Pulse count file %s action update",file_name);
    
 } else { /* timer based action */
   if (tst_app_timer(S0_CNT_TIM) == 0) { /* periodic update timer expired */
      set_app_timer(S0_CNT_TIM,MIN10);   /* reset to 10 minutes */
      write_pulsecnt_file(file_name,s0_pulse);
      syslog(LOG_INFO, "Pulse count file %s periodic update",file_name);
   }
 }
}

/**
 
 */
void printPulse(FILE *stream)
{
 float ttime;

 if (ttot > 0 && calc > 0) {
//          p0 = ((((WHour * 100) / PULSES_KWH) * 3600) / (ttot/100));
    ttime = ttot;
    p0 = (float)(((1000 * WHour) / pulses_kwh * 3600) / ttime);
    fprintf(stream, "tt0=%d, tt1=%d, S0 cnt=%d, Ecnt=%d, P0=%f , load=%d\n",
            tt0,tt1,s0_pulse, err_cnt, p0, load);
    calc = 0;
 }
}

/**
 * Read pulse count from storage
 */
int read_pulsecnt_file(char *file_name)
{
 FILE *conf_file = NULL;
 int ret = -1;
 int dummy;

 if (file_name == NULL) return 0;
 conf_file = fopen(file_name, "r");
 if (conf_file == NULL) {
    syslog(LOG_ERR, "Can not open pulse count file: %s, error: %s",
           file_name, strerror(errno));
    return -1;
 }
 ret = fscanf(conf_file, "%d", &dummy);
 if (ret > 0) {
    syslog(LOG_INFO, "Pulse count (%d) read from file %s", dummy,file_name);
 }
 fclose(conf_file);
 return (dummy);
}

/**
 * Write pulse count to storage
 */
int write_pulsecnt_file(char *file_name,unsigned int count)
{
 FILE *conf_file = NULL;
 int ret = -1;

 if (file_name == NULL) return 0;
 conf_file = fopen(file_name, "w");
 if (conf_file == NULL) {
    syslog(LOG_ERR, "Can not open pulse count file: %s, error: %s",
           file_name, strerror(errno));
    return -1;
 }
 ret = fprintf(conf_file, "%d\n", count);
 if (ret > 0) {
    syslog(LOG_INFO, "Pulse count (%d) written to file %s",count,file_name);
 }
 fclose(conf_file);
 return ret;
}

/**
 * Get the current load in Watt's
 */
unsigned long curr_load(void)
{
 return(load);
}

/**
 * Get the current S0 pulse count
 */
unsigned int pulse_cnt(void)
{
 return(s0_pulse);
}

/**
 * Get the current kWh count
 */
unsigned int kwh_cnt(void)
{
 return((s0_pulse * 1000) / pulses_kwh);
}

