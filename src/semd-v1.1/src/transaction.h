/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/
#ifndef TRANSACTION_H
#define TRANSACTION_H

int start_tr(dictionary *ini_fd, char *uid);
int stop_tr(dictionary *ini_fd, char *uid);
int write_tr(dictionary *ini_fd, char *uid);

#endif /* TRANSACTION_H */
