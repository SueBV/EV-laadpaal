/*
***************************************************************************
*
* Author: 
*
***************************************************************************
*
*
***************************************************************************
*/
#ifndef GPIO_H
#define GPIO_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <limits.h>

#define GPIO_ADDR  0x18040000 // GPIO register base address
#define GPIO_BLOCK 0x34       // GPIO register memory block size

/* GPIO registers */
#define GPIO_OE 0          // General Purpose I/O Output Enable
#define GPIO_IN 1          // General Purpose I/O Input Value
#define GPIO_OUT 2         // General Purpose I/O Output Value
#define GPIO_SET 3         //General Purpose I/O Bit Set
#define GPIO_CLEAR 4       //General Purpose I/O Per Bit Clear
#define GPIO_INT 5         //General Purpose I/O Interrupt Enable
#define GPIO_INT_TYPE 6    // General Purpose I/O Interrupt Type
#define GPIO_INT_POLARITY 7 // General Purpose I/O Interrupt Polarity
#define GPIO_INT_PENDING 8 // General Purpose I/O Interrupt Pending
#define GPIO_INT_MASK 9    // General Purpose I/O Interrupt Mask
#define GPIO_FUNCTION_1 10 // General Purpose I/O Function
#define GPIO_IN_ETH_SWITCH_LED 11 // General Purpose I/O Input Value
#define GPIO_FUNCTION_2 12 // Extended GPIO Function Control

#define OUT_GPIO(x) (*(gpioAddr + GPIO_OE) |= (1 << x))  // gpio as output
#define INP_GPIO(x) (*(gpioAddr + GPIO_OE) &= ~(1 << x)) // gpio as input
#define GPIO_HIGH(x) (*(gpioAddr + GPIO_SET) |= (1<<x)) // sets gpio pin high
#define GPIO_LOW(x) (*(gpioAddr + GPIO_CLR) |= (1<<x)) // sets gpio pin low
#define GET_GPIO(x) (*(gpioAddr + GPIO_IN)&(1<<x)) // 0 if low, (1<<x) if high
#define SET_GPIO(x) (*(gpioAddr + GPIO_OUT) |= (1<<x)) // set gpio pin high
#define CLR_GPIO(x) (*(gpioAddr + GPIO_OUT) &= ~(1<<x)) // set gpio pin low
#define TGL_GPIO(x) (*(gpioAddr + GPIO_OUT) ^= (1<<x)) // toggle gpio pin
#define RD_GPIO     (*(gpioAddr + GPIO_IN)) // read gpio input value (state)
#define WR_GPIO     (*(gpioAddr + GPIO_OUT)) // write gpio output value (state)

int gpioSetup(void);
char *int2bin(unsigned , char *);
void printGPIOreg(FILE *);

#endif /* GPIO_H */
