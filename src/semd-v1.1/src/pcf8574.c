/*
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include "timer.h"
#include "pcf8574.h"

static int pcf8574_fd = -1;
static uint8_t pcf8574_inp = 0x00;
static uint8_t pcf8574_out = 0xff;

static struct beeper {
  int busy;
  int period;
  unsigned int on_time;
  unsigned int off_time;
  int repeat;
} beeber;


/*
 *
 */
int init_i2c_io(char *filename, int slave_addr)
{
 int res = -1;
 int fd = -1;
 int i;

 if ((fd = open(filename, O_RDWR)) < 0) { // Open port for reading and writing
    printf("Failed to open i2c port\n");
    return(fd);
 } else {
    if (res = ioctl(fd, I2C_SLAVE, slave_addr) < 0) { // Set the port options and set the address of the device we wish to speak to
       printf("Unable to get bus access to slave %0x\n",slave_addr);
       return(res);
    }
 }
 if (read(fd,&pcf8574_inp, 1) < 0) {
    printf("Error reading PCF8574 device\n");
    return(-1);
 }
 if (write(fd,&pcf8574_out,1) < 0) {
    printf("Error writing PCF8574 device\n");
    return(-1);
 }
 pcf8574_fd = fd;
 return(fd);
}


/*
 *
 */
uint8_t i2c_io_inp(void)
{
 if (pcf8574_fd > 0)
    read(pcf8574_fd, &pcf8574_inp, 1);
 return(pcf8574_inp);
}

/*
 *
 */
int i2c_io_out(uint8_t val)
{
 int res = -1;

 pcf8574_out = val;
 if (pcf8574_fd > 0)
    res = write(pcf8574_fd, &pcf8574_out, 1);
 return(res);
}

/*
 *
 */
void init_beeber(void)
{
 beeber.busy = 0;
 beeber.period = 0;
 beeber.on_time = 0;
 beeber.off_time = 0;
 beeber.repeat = 0;
}

/*
 *
 */
void beeber_handler(void)
{
 if (beeber.busy > 0) {
    if (beeber.period == 0) {  // init beep
       beeber.period = 1;
       set_app_timer(BEEP_TIM,beeber.on_time);
       i2c_io_out(0x00); // beeber(on);
    }
    if (beeber.period == 1) { // beep on time
       if (tst_app_timer(BEEP_TIM) == 0) {
          beeber.period = 2;
          set_app_timer(BEEP_TIM, beeber.off_time);
          i2c_io_out(0x01); // beeber(off);
       }
    }
    if (beeber.period == 2) { // beep off time
       if (tst_app_timer(BEEP_TIM) == 0) {
          beeber.period = 3;
       }
    }
    if (beeber.period == 3) { // beep repeat
          beeber.period = 0;
       if (beeber.repeat > 1) {
          beeber.repeat--;
       } else {
          beeber.busy = 0;
          beeber.repeat = 0;
       }
    }
 }
}

/*
 *
 */
void beeb(int times,unsigned int on,unsigned int off)
{
 if (beeber.busy == 0) {
    beeber.busy = 1;
    beeber.period = 0;
    beeber.on_time = on;
    beeber.off_time = off;
    beeber.repeat = times;
 }
}

