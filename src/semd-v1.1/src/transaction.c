/*
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include <time.h>
#include "pulse.h"
#include "iniparser.h"
#include "transaction.h"

static FILE *tr_log_fd;
static char *tr_data_dir;
static char *tr_log_name;

static struct transaction {
   char *id;
   char tr_id[40];
   char nfc_id[10];
   time_t t_start;
   time_t t_stop;
   unsigned int c_start;
   unsigned int c_stop;
   unsigned int wh_tot;
} tr ;


/*
 *
 */
int start_tr(dictionary *ini_fd, char *uid)
{
 char *tmp;
 time_t t = time(NULL);
 struct tm tm = *localtime(&t);

 if ((tr_log_name = iniparser_getstring(ini_fd,"transaction:tr_log_file",NULL)) == NULL )
    return(-1);
 if ((tr_data_dir = iniparser_getstring(ini_fd,"transaction:tr_data_dir",NULL)) == NULL )
    return(-2);
 if ((tr.id = iniparser_getstring(ini_fd,"owner:id",NULL)) == NULL )
    return(-3);
 if ((tmp = iniparser_getstring(ini_fd,"transaction:tr_id",NULL)) == NULL )
    return(-4);
// sprintf(tr.tr_id,"%-11s:%04d%02d%02d%02d%02d%02d",tmp,tm.tm_year + 1900,tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
 sprintf(tr.tr_id,"%s-%04d%02d%02d%02d%02d%02d",tmp,tm.tm_year + 1900,tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
 strcpy(tr.nfc_id,uid);
 tr.t_start = time(NULL);
 tr.c_start = kwh_cnt();
 if ((tr_log_fd = fopen(tr_log_name,"a+")) == NULL) // Open logfile
    return(-5);
 fprintf(tr_log_fd,"%d-%d-%d %d:%d:%d->", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
 fprintf(tr_log_fd,"start,%s,%s,%d,%s\n",tr.tr_id,tr.id,tr.c_start,tr.nfc_id);
 if (fclose(tr_log_fd) != 0)
    return(-6);
 return(1);
}


/*
 *
 */
int stop_tr(dictionary *ini_fd, char *uid)
{
 time_t t = time(NULL);
 struct tm tm = *localtime(&t);

 tr.t_stop = time(NULL);
 tr.c_stop = kwh_cnt();
 tr.wh_tot = tr.c_stop - tr.c_start;

 if ((tr_log_fd = fopen(tr_log_name,"a+")) == NULL) // Open logfile
    return(-1);
 fprintf(tr_log_fd,"%d-%d-%d %d:%d:%d->", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
 fprintf(tr_log_fd,"stop,%s,%s,%d,%d,%s\n",tr.tr_id,tr.id,tr.c_stop,tr.wh_tot,tr.nfc_id);
 if (fclose(tr_log_fd) != 0)
    return(-2);
 return(1);
}


/*
 *
 */
int write_tr(dictionary *ini_fd, char *uid)
{
 char tmp[256];
 struct tm tm;
 unsigned int t;
 FILE *fd;

 sprintf(tmp,"%s/%s",tr_data_dir,tr.tr_id); // contruct filename
 if ((fd = fopen(tmp,"a+")) == NULL) // Open transaction file
    return(-1);
 fprintf(fd,"%s,%s,",tr.id,tr.nfc_id); // Our ID, NFC-tag id (name)
 tm = *localtime(&tr.t_start);
 fprintf(fd,"%02d-%02d-%04d ",tm.tm_mday,tm.tm_mon + 1,tm.tm_year + 1900);
 fprintf(fd,"%02d:%02d:%02d,",tm.tm_hour,tm.tm_min,tm.tm_sec);
 t = tr.t_stop - tr.t_start;
 fprintf(fd,"%02d:%02d:%02d,",(t/3600),((t%3600)/60),((t%3600)%60));
 fprintf(fd,"%d.%03d\n",tr.wh_tot/1000,tr.wh_tot%1000);
 if (fclose(fd) != 0)
    return(-2);
 return(1);
}

