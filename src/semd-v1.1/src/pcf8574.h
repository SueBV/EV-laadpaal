/*
  pcf8574.h
*/
#ifndef _PCF8574_H_
#define _PCF8574_H_

// default i2c address
#define PCF8574_I2C_ADDR 0x20   // PCF8574 I2C Slave address

/* PCF8574 registers */
#define REG_OUT	0	// write
#define REG_INP	0	// read


/* functions in pcf8574.c */
int init_i2c_io(char *filename, int slave_addr);
uint8_t i2c_io_inp(void);
int i2c_io_out(uint8_t val);
void init_beeber(void);
void beeber_handler(void);
void beeb(int times,unsigned int on,unsigned int off);



#endif /* _PCF8574_H_ */
/*
#define OUT_GPIO(x) (*(gpioAddr + GPIO_OE) |= (1 << x))  // gpio as output
#define INP_GPIO(x) (*(gpioAddr + GPIO_OE) &= ~(1 << x)) // gpio as input
#define GPIO_HIGH(x) (*(gpioAddr + GPIO_SET) |= (1<<x)) // sets gpio pin high
#define GPIO_LOW(x) (*(gpioAddr + GPIO_CLR) |= (1<<x)) // sets gpio pin low
#define GET_GPIO(x) (*(gpioAddr + GPIO_IN)&(1<<x)) // 0 if low, (1<<x) if high
#define SET_GPIO(x) (*(gpioAddr + GPIO_OUT) |= (1<<x)) // set gpio pin high
#define CLR_GPIO(x) (*(gpioAddr + GPIO_OUT) &= ~(1<<x)) // set gpio pin low
#define TGL_GPIO(x) (*(gpioAddr + GPIO_OUT) ^= (1<<x)) // toggle gpio pin
#define RD_GPIO     (*(gpioAddr + GPIO_IN)) // read gpio input value (state)
#define WR_GPIO     (*(gpioAddr + GPIO_OUT)) // write gpio output value (state)
*/

