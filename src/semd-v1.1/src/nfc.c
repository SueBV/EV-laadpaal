/*
 *
 */

#include <stdlib.h>
#include "nfc.h"

static nfc_context *context;
static nfc_target nt;


void sprint_hex(uint8_t *buf, const uint8_t *pbtData, const size_t szBytes)
{
 size_t  szPos;

 for (szPos = 0; szPos < szBytes; szPos++) {
    sprintf(&(buf[szPos*2]),"%02x", pbtData[szPos]);
 }
}

/*
 *
 *
 */
nfc_device * init_nfc(void)
{
 nfc_device *pnd;
 nfc_init(&context); // Initialize libnfc and set the nfc_context
 if (context == NULL) {
    printf("Unable to init libnfc (malloc)\n");
    return(NULL);
 }

 // Display libnfc version
 const char *acLibnfcVersion = nfc_version();
// printf("Using libnfc %s\n", acLibnfcVersion);

 // Open, using the first available NFC device which can be in order of selection:
 //   - default device specified using environment variable or
 //   - first specified device in libnfc.conf (/etc/nfc) or
 //   - first specified device in device-configuration directory (/etc/nfc/devices.d) or
 //   - first auto-detected (if feature is not disabled in libnfc.conf) device
 pnd = nfc_open(context, NULL);
 if (pnd == NULL) {
    printf("ERROR: %s\n", "Unable to open NFC device.");
    return(NULL);
 }
 // Set opened NFC device to initiator mode
 if (nfc_initiator_init(pnd) < 0) {
    nfc_perror(pnd, "nfc_initiator_init");
    return(NULL);
 }
 printf("NFC reader: %s opened\n", nfc_device_get_name(pnd));
 return(pnd);
}

/*
 *
 *
 */
int poll_nfc(nfc_device *dev)
{
 const uint8_t uiPollNr = 1;
 const uint8_t uiPeriod = 1;
 const nfc_modulation nmModulations[5] = {
    { .nmt = NMT_ISO14443A, .nbr = NBR_106 },
    { .nmt = NMT_ISO14443B, .nbr = NBR_106 },
    { .nmt = NMT_FELICA, .nbr = NBR_212 },
    { .nmt = NMT_FELICA, .nbr = NBR_424 },
    { .nmt = NMT_JEWEL, .nbr = NBR_106 },
 };
 const size_t szModulations = 1; /* use only the first tag type */

 if (nfc_initiator_poll_target(dev, nmModulations, szModulations, uiPollNr, uiPeriod, &nt) > 0) { /* found a NFC tag */
//    printf("UID (NFCID%c): ", (nt.nti.nai.abtUid[0] == 0x08 ? '3' : '1'));
//    print_hex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
    return(1);
 }
 return(0);
}

/*
 *
 *
 */
int scan_nfc(nfc_device *dev)
{
 // Poll for a ISO14443A (MIFARE) tag
 const nfc_modulation nmMifare = {
    .nmt = NMT_ISO14443A,
    .nbr = NBR_106,
 };
 if (nfc_initiator_select_passive_target(dev, nmMifare, NULL, 0, &nt) > 0) { /* found a NFC tag */
//  if (nfc_initiator_select_passive_target_ext(dev, nmMifare, NULL, 0, &nt, 100) > 0) { /* found a NFC tag */
//  if (pn53x_initiator_select_passive_target_ext(dev, nmMifare, NULL, 0, &nt, 100) > 0) { /* found a NFC tag */
//      printf("UID (NFCID%c): ", (nt.nti.nai.abtUid[0] == 0x08 ? '3' : '1'));
//      print_hex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
    return(1);
 }
 return(0);
}

/*
 *
 *
 */
int close_nfc(nfc_device *dev)
{
// nfc_abort_command(dev);
 // Close NFC device
 nfc_close(dev);
 // Release the context
 nfc_exit(context);
 return(EXIT_SUCCESS);
}

/*
 *
 *
 */
int get_tag_id(uint8_t * tag)
{
 int i;

 for (i=0; i<10; i++)
    tag[i] = nt.nti.nai.abtUid[i];
 return(nt.nti.nai.szUidLen);
}

