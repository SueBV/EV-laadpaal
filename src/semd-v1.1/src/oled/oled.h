/*
  OLED display
  driver for the sh1106 and ssd1306 display controllers
*/
#ifndef _OLED_H_
#define _OLED_H_

//#define SSD1306
#define SH1106

#ifdef SH1106
#define OFFSET 0x02       // offset=2 for SH1106 controller
//#define OFFSET 0x00       // offset=2 for SH1106 controller
#else
#define OFFSET 0x00       // offset=0 for SSD1306 controller
#endif

#define BLACK 0
#define WHITE 1
#define INVERSE 2

#define WIDTH_POS 0
#define HEIGHT_POS 1
#define FIRST_CHAR_POS 2
#define CHAR_NUM_POS 3
#define CHAR_WIDTH_START_POS 4

#define TEXT_ALIGN_LEFT 0
#define TEXT_ALIGN_CENTER 1
#define TEXT_ALIGN_RIGHT 2

//display dimensions - the physical LCD in pixels
#ifdef SSD1306
#define LCDWIDTH   128  // SSD1306
#else
#define LCDWIDTH   132  // SH1106
#endif
#define LCDHEIGHT  64

// default i2c address
#define OLED_address 0x3c // OLED's I2C slave address

/* registers */
#define COMM_MODE       0x80    // Command mode
#define DATA_MODE       0x40    // Data mode

/* commands */
#define EXTERNALVCC 0x01
#define SWITCHCAPVCC 0x02
#define SETLOWCOLUMN 0x00
#define SETHIGHCOLUMN 0x10
#define MEMORYMODE 0x20
#define COLUMNADDR 0x21
#define PAGEADDR 0x22
#define SETSTARTLINE 0x40
#define SETCONTRAST 0x81
#define CHARGEPUMP 0x8D
#define SEGREMAP    0xA0
#define SETSEGMENTREMAP 0xA1
#define DISPLAYALLON_RESUME 0xA4
#define DISPLAYALLON 0xA5
#define NORMALDISPLAY 0xA6
#define INVERTDISPLAY 0xA7
#define SETMULTIPLEX 0xA8
#define DISPLAYOFF 0xAE
#define DISPLAYON 0xAF
#define PAGESTARTADDRESS 0xB0
#define COMSCANINC 0xC0
#define COMSCANDEC 0xC8
#define SETDISPLAYOFFSET 0xD3
#define SETDISPLAYCLOCKDIV 0xD5
#define SETPRECHARGE 0xD9
#define SETCOMPINS 0xDA
#define SETVCOMDETECT 0xDB

/* functions in oled.c */
int oled_init(char *filename, int slave_addr);
int oled_send_command(int fd, uint8_t command);
int oled_send_cmdstr(int fd, uint8_t * data, int len);
int oled_send_data(int fd, uint8_t * data, int len);
int oled_reset(int fd);
void oled_reset_display(int fd);
int oled_clear(int fd);
int oled_setpos(int fd, uint8_t x, uint8_t y);
int oled_displayOn(int fd);
int oled_displayOff(int fd);
int oled_setContrast(int fd, uint8_t contrast);
int oled_fill4(int fd, uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4);
int oled_fill2(int fd, uint8_t p1, uint8_t p2);
int oled_fill(int fd, uint8_t p);
int oled_char_font6x8(int fd, char ch);
int oled_string_font6x8(int fd, char *s);
int oled_numdec_font6x8(int fd, uint16_t num);
int oled_numdecp_font6x8(int fd, uint16_t num);
int oled_string_font8x16xy(int fd, uint8_t x, uint8_t y, const char s[]);
int oled_draw_bmp(int fd, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, const uint8_t bitmap[]);

#define oled_char(f,c) oled_char_font6x8(f,c)
#define oled_string(f,s) oled_string_font6x8(f,s)
#define oled_numdec(f,n) oled_numdec_font6x8(f,n)
#define oled_numdecp(f,n) oled_numdecp_font6x8(f,n)

#endif /* _OLED_H_ */

