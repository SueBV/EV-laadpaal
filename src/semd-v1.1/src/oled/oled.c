//
// sh1106/ssd1306 oled display functions
//
// Displays text on the OLED screen.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include "oled.h"
#include "num2str.h"


#include "font6x8.h"
#include "font8x16.h"

#define RAM_BUFFER_SIZE (((LCDWIDTH * LCDHEIGHT) / 8) + 4)

static uint8_t oled_cmdbuf[8];
static uint8_t oled_datbuf[1+132];

const uint8_t oled_init_seq [] = {      // Initialization Sequence
// COMM_MODE,
// DISPLAYOFF,               // 0xAE
// NORMALDISPLAY,            // 0xA6
// SETDISPLAYCLOCKDIV, 0x80, // 0xD5,0x80

// SETMULTIPLEX, 0x3F,       // 0xA8,0x3F
// SETDISPLAYOFFSET, 0x00,   // 0xD3,0x00
// SETSTARTLINE | 0x00,      // 0x40
// CHARGEPUMP, 0x14,         // 0x8D,0x14
// MEMORYMODE, 0x00,         // 0x20,0x00
//// SEGREMAP,                 // 0xA0
// SETSEGMENTREMAP,          // 0xA1
// COMSCANINC,               // 0xC0
// SETCOMPINS, 0x12,         // 0xDA,0x12
// SETCONTRAST, 0xCF,        // 0x81,0xCF
// SETPRECHARGE, 0xF1,       // 0xD9,0xF1
// SETVCOMDETECT, 0x40,      // 0xDB,0x40
// DISPLAYALLON_RESUME,      // 0xA4
// NORMALDISPLAY,            // 0xA6
//
 DISPLAYOFF,               // 0xAE
 COMSCANDEC,               // 0xC8
 SETSEGMENTREMAP,          // 0xA1
#ifdef SSD1306
 CHARGEPUMP, 0x14,         // 0x8D,0x14 Set DC-DC enable
 DISPLAYON,                // 0xAF
#endif

 
};

/*
 *
 */
int oled_init(char *filename, int slave_addr)
{
 int res = -1;
 int fd = -1;

 if ((fd = open(filename, O_RDWR)) < 0) { // Open port for reading and writing
     printf("Failed to open i2c port\n");
     return(fd);
 } else {
     if (res = ioctl(fd, I2C_SLAVE, slave_addr) < 0) { // Set the port options and set the address of the device we wish to speak to
         printf("Unable to get bus access to slave %0x\n",slave_addr);
         return(res);
     }
 }
 memset(oled_cmdbuf, 0, sizeof(oled_cmdbuf));
 memset(oled_datbuf, 0, sizeof(oled_datbuf));
 if ( (res = oled_reset(fd)) < 0)
    printf("Error initialising OLED screen: err=%d\n", res);
 return(fd);
}


/*
 *
 */
int oled_send_command(int fd, uint8_t command)
{
 oled_cmdbuf[0] = COMM_MODE;
 oled_cmdbuf[1] = command;
 return (write(fd, oled_cmdbuf, 2));
}

/*
 *
 */
int oled_send_cmdstr(int fd, uint8_t * data, int len)
{
 return (write(fd, data, len));
}

/*
 *
 */
int oled_send_data(int fd, uint8_t * data, int len)
{
 return (write(fd, data, len));
}

/*
 *
 */
int oled_reset(int fd)
{
 int i;
 int res;

 for (i=0; i<sizeof(oled_init_seq); i++) {
    if ((res = oled_send_command(fd,oled_init_seq[i])) < 0)
       return(res);
 }
 return(res);
}

/*
 *
 */
void oled_reset_display(int fd)
{
 oled_displayOff(fd);
 oled_clear(fd);
 oled_displayOn(fd);
}

/*
 * clears the display by filling the display RAM with zero's
 */
int oled_clear(int fd)
{
 int row;

/* prepare the display data */
 memset(oled_datbuf, 0, sizeof(oled_datbuf));
 oled_datbuf[0] = DATA_MODE;

/* prepare the cmd data */
 oled_cmdbuf[0] = COMM_MODE;
 oled_cmdbuf[2] = COMM_MODE;
 oled_cmdbuf[3] = SETLOWCOLUMN;  /* col = 0 */
 oled_cmdbuf[4] = COMM_MODE;
 oled_cmdbuf[5] = SETHIGHCOLUMN;

 for (row = 0; row < 8; row++) {
    oled_cmdbuf[1] = (PAGESTARTADDRESS + row);
    if (oled_send_cmdstr(fd, oled_cmdbuf, 6) <= 0)
       return(0); 
    if (oled_send_data(fd,oled_datbuf,sizeof(oled_datbuf)) <= 0)
       return(0);
 }
 return(1);
}

/*
 * Set cursor in a 16 COL x 8 ROW map
 */
int oled_setpos(int fd, uint8_t xcol, uint8_t yrow)
{
 oled_cmdbuf[0] = COMM_MODE;
 oled_cmdbuf[1] = (PAGESTARTADDRESS + yrow);
 oled_cmdbuf[2] = COMM_MODE;
 oled_cmdbuf[3] = SETLOWCOLUMN + ((OFFSET + 8 * xcol) & 0x0F);
 oled_cmdbuf[4] = COMM_MODE;
 oled_cmdbuf[5] = SETHIGHCOLUMN + (((OFFSET + 8 * xcol) >> 4) & 0x0F);
 return (oled_send_cmdstr(fd, oled_cmdbuf, 6));
}

/*
 *
 */
int oled_displayOn(int fd)
{
 return (oled_send_command(fd,DISPLAYON));
}

/*
 *
 */
int oled_displayOff(int fd)
{
 return (oled_send_command(fd,DISPLAYOFF));
}

/*
 *
 */
int oled_setContrast(int fd, uint8_t contrast)
{
 oled_cmdbuf[0] = COMM_MODE;
 oled_cmdbuf[1] = SETCONTRAST;
 oled_cmdbuf[2] = contrast;
 return (oled_send_cmdstr(fd, oled_cmdbuf, 3));
}

/*
 *
 */
int oled_fill4(int fd, uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4)
{
 int i;
 int row;

/* build the fill pattern */
 oled_datbuf[0] = DATA_MODE;
 for (i = 0; i < 128 / 4; i++) {
    oled_datbuf[(i*4)+1] = p1;
    oled_datbuf[(i*4)+2] = p2;
    oled_datbuf[(i*4)+3] = p3;
    oled_datbuf[(i*4)+4] = p4;
 }
 for (row = 0; row < 8; row++) {
    oled_setpos(fd, 0, row);
    if (oled_send_data(fd,oled_datbuf,sizeof(oled_datbuf)) <= 0)
       return(0);
 }
 return (1);
}

/*
 *
 */
int oled_fill2(int fd, uint8_t p1, uint8_t p2)
{
 return(oled_fill4(fd, p1, p2, p1, p2));
}

/*
 *
 */
int oled_fill(int fd, uint8_t p)
{
 return(oled_fill4(fd, p, p, p, p));
}

/*
 *
 */
int oled_char_font6x8(int fd, char ch)
{
 uint8_t c = ch - 32;
 int i;

 oled_datbuf[0] = DATA_MODE;
 for (i = 0; i < 6; i++) 
    oled_datbuf[1+i] = oled_font6x8[c * 6 + i];
 return(write(fd, oled_datbuf, 1+i));
}

/*
 *
 */
int oled_string_font6x8(int fd, char *s)
{
 int res;

 while (*s) {
    if ((res = oled_char_font6x8(fd, *s++)) < 0)
       return(res);
 }
 return(res);
}

char oled_numdec_buffer[USINT2DECASCII_MAX_DIGITS + 1];

/*
 *
 */
int oled_numdec_font6x8(int fd, uint16_t num)
{
 uint8_t digits;

 oled_numdec_buffer[USINT2DECASCII_MAX_DIGITS] = '\0'; // Terminate the string.
 digits = usint2decascii(num, oled_numdec_buffer);
 return(oled_string_font6x8(fd, oled_numdec_buffer + digits));
}

/*
 *
 */
int oled_numdecp_font6x8(int fd, uint16_t num)
{
 oled_numdec_buffer[USINT2DECASCII_MAX_DIGITS] = '\0'; // Terminate the string.
 usint2decascii(num, oled_numdec_buffer);
 return(oled_string_font6x8(fd, oled_numdec_buffer));
}

/*
 *
 */
int oled_string_font8x16xy(int fd, uint8_t x, uint8_t y, const char s[])
{
 uint8_t ch, i, j = 0;
 int res;

 while (s[j] != '\0') {
    ch = s[j] - 32;
    if (x > 16) {
       x = 0;
       y++;
    }
    oled_setpos(fd, x, y);
    oled_datbuf[0] = DATA_MODE;
    for (i = 0; i < 8; i++) {
       oled_datbuf[1+i] = oled_font8x16[ch * 16 + i];
    }
    if ((res = write(fd, oled_datbuf, 1+i)) < 0)
       return(res);
    oled_setpos(fd, x, y + 1);
    oled_datbuf[0] = DATA_MODE;
    for (i = 0; i < 8; i++) {
       oled_datbuf[1+i] = oled_font8x16[ch * 16 + i + 8];
    }
    if ((res = write(fd, oled_datbuf, 1+i)) < 0)
       return(res);
    x += 1;
    j++;
 }
}

/*
 *
 */
int oled_draw_bmp(int fd, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, const uint8_t bitmap[])
{
 int res;
 uint16_t j = 0;
 uint8_t y, x;

 if (y1 % 8 == 0)
    y = y1 / 8;
 else
    y = y1 / 8 + 1;
 for (y = y0; y < y1; y++) {
    oled_setpos(fd,x0,y);
    oled_datbuf[0] = DATA_MODE;
    for (x = x0; x < x1; x++) 
       oled_datbuf[1+x] = bitmap[j++];
    if ((res = write(fd, oled_datbuf, 1+x)) < 0)
       return(res);
 }
 return(res);
}

